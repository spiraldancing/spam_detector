﻿using System;
using System.IO.Enumeration;
using System.Linq;
using System.Text.RegularExpressions;

namespace SpamDetector
{
    class Program
    {

        static void Main()
        {
            string[] emailFileNames = Directory.GetFiles(@"data_input/emails_to_test");
            Array.Sort(emailFileNames);     // Because, for some reason, it does not load the filenames in order.
            List<string> emailRawFiles = new();
            List<string> emailFiles = new();

            foreach (string fileName in emailFileNames)
            {
                emailRawFiles.Add(File.ReadAllText(fileName));
            }

            // Start the report to document which email pairs appear to be spam.
            StreamWriter report = new StreamWriter("data_output/Spam_Report.txt");
            report.WriteLine("Spam Email Analysis Report");
            report.WriteLine("Analyzing {0} emails located in the 'data_input/emails_to_test' Directory.\n\n");

            // Some minor string-sanitizing, converting the text into a pure space-separated stream of words.
            string cleanFile;
            foreach (string email in emailRawFiles)
            {
                cleanFile = Regex.Replace(email, @"\s+", " ");    // Replace all whitespace with single ' ' character

                // Strip out most common punctuation. NB: Does *not* remove apostrophe (') symbol, in order to preserve contractions 
                //  and possessives.
                cleanFile = cleanFile.Trim(new Char[] {',', '.', ';', '?', '!', ':', '"', '(', ')', '-', '_' });
                cleanFile = cleanFile.ToLower();    // Convert to all-lowercase
                emailFiles.Add(cleanFile);
            }

            // Begin analysis here. Compare all possible email-pairs and include the most suspicious pairs in the report.
            // TODO: Once this loop identifies, eg, emails 0001 and 0025 as a spam-pair, it should exclude 0025 from the primary loop. etc.
            for (int i = 0; i < (emailFiles.Count - 1); i++)
            {
                for (int j = i+1; j < emailFiles.Count; j++)
                {
                    double f1Length = (double)emailFiles[i].Length;
                    double f2Length = (double)emailFiles[j].Length;
                    double lengthRatio = (f1Length < f2Length) ? (f1Length / f2Length)  : (f2Length / f1Length);

                    // Prefilter #1: If the length of two emails is very different (ratio <= 0.8), assume they are *not* near-duplicates.
                    if (lengthRatio > 0.8)
                    {
                        // Prefilter #2: If a simple "Bag of Words" comparison shows very different content ('Bag of Words'
                        //  similarity <= 0.7), assume they are *not* near-duplicates.
                        double bowSimilarity = BagOfWordsPreFilter(emailFiles[i], emailFiles[j]);
                        if (bowSimilarity > 0.7)
                        {
                            // TODO: Prefilter #3 is not implemented ... may not be necessary.
                            // Prefilter #3: If a Bigram (2-word n-gram) comparison shows very different content (Bigram 
                            //  similarity <= 0.7), assume they are *not* near-duplicates.
                            if ((BiGramPreFilter(emailFiles[i], emailFiles[j])) > 0.7)
                            {
                                // Filter #4: Check the Levenshtein Distance. If the ratio of this, to the maximum length of the larger
                                //  email, is very far off (ratio < 0.2), we have a winner ... this pair of emails is quite likely a 
                                //  near-duplicate spam email.
                                int levDis = LevenshteinDistance(emailFiles[i], emailFiles[j]);
                                double levRatio = (f1Length < f2Length) ? (levDis / f2Length)  : (levDis / f1Length);
                                if (levRatio < 0.2)
                                {
                                    Console.WriteLine("For emails " + (i + 1).ToString("0000") + " and " + (j + 1).ToString("0000") + ", the size ratio is " 
                                        + lengthRatio.ToString("0.000") + "; the BagOfWords similarity is " + bowSimilarity.ToString("0.000") 
                                        + "; the Levenshtein Ratio is " + levRatio.ToString("0.000") + ". This pair is probably spam.");
                                    report.WriteLine("For emails " + (i + 1).ToString("0000") + " and " + (j + 1).ToString("0000") + ", the size ratio is " 
                                        + lengthRatio.ToString("0.000") + "; the BagOfWords similarity is " + bowSimilarity.ToString("0.000") 
                                        + "; the Levenshtein Distance is " + levDis.ToString() + ". This pair is probably spam.");
                                }
                            }
                        }
                    }
                }        
            }
        }


        static double BagOfWordsPreFilter(string a, string b)
        {

            // TODO: This method builds a potentially very useful Bag of Words collection for each given email file,
            //  however, it is only doing a very rudimentary comparison of the results. A better analysis of the differences
            //  could still be implemented.

            // TODO: This method should be split into two separate methods.
            //      Method 1 should generate the Bag of Word Dictionaries (perhaps refactored/abstracted to produce n-gram 
            //  Dictionaries to also be used by the Bigram, and perhaps other n-gram Filter methods).
            //      Method 2 should handle the comparison of the pre-generated Dictionaries.

            var aArray = a.Split(' ').ToList();
            var bArray = b.Split(' ').ToList();
            var bowA = new Dictionary<string, int>();
            var bowB = new Dictionary<string, int>();

            // Convert email file 'A' into a simple Bag of Words
            foreach (string nextWord in aArray)
            {
                if (bowA.ContainsKey(nextWord))
                {
                    bowA[nextWord] += 1;
                }
                else
                {
                    bowA.Add(nextWord, 1);
                }
            }

            // Convert email file 'B' into a simple Bag of Words
            foreach (string nextWord in bArray)
            {
                if (bowB.ContainsKey(nextWord))
                {
                    bowB[nextWord] += 1;
                }
                else
                {
                    bowB.Add(nextWord, 1);
                }
            }

            // Do an intersection of BagOfWords-A and BoW-B to calculate how similar they are.
            HashSet<string> sharedKeys = new HashSet<string>(bowA.Keys);
            sharedKeys.IntersectWith(bowB.Keys);
            var sharedWords = 
                bowA
                .Where(x => sharedKeys.Contains(x.Key))
                .Concat(bowB.Where(x => sharedKeys.Contains(x.Key)))
                // .Select(x => x.Value) // With this additional select you'll get only the values.
                .ToList();            

            int allAWordsCount = aArray.Count();
            int allBWordsCount = bArray.Count();
            aArray.RemoveAll(item => sharedKeys.Contains(item));
            bArray.RemoveAll(item => sharedKeys.Contains(item));

            double uniqueARatio = 1.0 - ((double)aArray.Count() / (double)allAWordsCount);
            double uniqueBRatio = 1.0 - ((double)bArray.Count() / (double)allBWordsCount);

            double abUniquenessRatio = ((uniqueARatio + uniqueBRatio) / 2.0);

            return abUniquenessRatio;

        }


        static double BiGramPreFilter(string a, string b)
        {
            // TODO: Placeholder. Functionally quite similar to the BagOfWordsPreFilter; can start with that.
            //  This prefilter may not be necessary. On the other hand, it might be sufficiently accurate to eliminate
            //  the Levenshtein test entirely, so worth implementing to try it.

            return 0.99;
        }


        static int LevenshteinDistance(string s, string t)
        {
            int sCount = s.Length;
            int tCount = t.Length;
            int[,] lDistance = new int[sCount + 1, tCount + 1];
		
            if (sCount == 0)
            {
                return tCount;
            }
        
            if (tCount == 0)
            {
                return sCount;
            }

            for (int i = 0; i <= sCount; i++)
                lDistance[i, 0] = i;
            for (int j = 0; j <= tCount; j++)
                lDistance[0, j] = j;
			
            for (int j = 1; j <= tCount; j++)
                for (int i = 1; i <= sCount; i++)
                    if (s[i - 1] == t[j - 1])
                        lDistance[i, j] = lDistance[i - 1, j - 1];  //no operation
                    else
                        lDistance[i, j] = Math.Min(
                            Math.Min(
                                lDistance[i - 1, j] + 1,            //a deletion
                                lDistance[i, j - 1] + 1),           //an insertion
                                lDistance[i - 1, j - 1] + 1         //a substitution
                            );
            return lDistance[sCount, tCount];
        }
    }
}


// Thinking this through

// Step 1:  User supply location of email dataset. For simplicity, limit to just local filesystem
// Step 2:  Load full email dataset
// Step 2.5:    Perhaps do some basic string sanitization -- remove excess whitespace, remove punctuation, switch to lowercase.
// Step 3:  Start a report builder ... continue appending to it incrementally as analysis progresses
// Step 4:  The algorithm.
//      A)  This will run sequentially through the entire dataset, except perhaps skipping any emails that have already been flagged as 
//          spam-likely near-clones of an earlier email.
//      B)  First do a simple pre-filter:  Check byte-length of each email file; skips files with > 20% size difference.
//          That seems like a very fast-and-easy check.
//      C)  Second pre-filter: do a simple Bag of Words analysis ... skips files without a significant overlap in common words (60+%?).
//      D)  Finally, run a full LevenshteinDistance analysis on email pairs that pass both pre-filters.

//      E)  Consider another pre-filter using a Bigram version of the Bag of Words pre-filter. Also consider the possibility that with
//          one or both n-gram "pre-" filters, the final Levenshtein test may not even be necessary.

