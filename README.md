# spam_detector

A practice exercise for a potential employer.

A spam detector that analyzes a collection of email bodies, and rates the likelihood each is spam, based on similarity to the others ... based on the theory that spam emails are often nearly-identical, only changed slightly to help avoid detectors like this one.

NB: This is a work-in-progress that may or may not be completed at a later date. If anyone is actually interested in details, feel free to drop me a line to discuss.

Currently, it includes a simple "file-size comparison" pre-filter, and a slightly more effective "Bag of Words" pre-filter, and then for email-pairs that pass both of those tests, a final Levenshtein Distance filter.

The Bag of Words filter is still rudimentary, and there is a placeholder for a 3rd-level pre-filter based on a modified Bag of Words Bigram filter.

My Python Email_Generator is designed to produce testing data for this Spam Detector.